package by.epamlab.jsptask.tags;

import javax.servlet.ServletContext;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;

/**
 * Created by Andrei Halauneu on 02.08.2016.
 */
public class ResolveURLTag extends SimpleTagSupport {
    private static final String KEY_URL = "url";
    private static final String KEY_RESOURCE = "resource";

    private String url;

    /**
     * Set url
     * @param url attribute
     */
    public void setUrl(final String url) {
        this.url = url;
    }

    @Override
    public void doTag() throws JspException, IOException {
        PageContext pageContext = (PageContext) getJspContext();
        ServletContext servletContext = pageContext.getServletContext();
        String path = servletContext.getRealPath(url);
        pageContext.setAttribute(KEY_URL, url);
        pageContext.setAttribute(KEY_RESOURCE, path);
    }
}

