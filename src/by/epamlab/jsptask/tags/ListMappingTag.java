package by.epamlab.jsptask.tags;

import javax.servlet.ServletContext;
import javax.servlet.ServletRegistration;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by Andrei Halauneu on 02.08.2016.
 */
public class ListMappingTag extends TagSupport {
    private static final String KEY_URL = "url";
    private static final String KEY_SERVLET = "servlet";

    private Map<String, ? extends ServletRegistration> registrationMap;
    private Iterator iterator;

    @Override
    public int doStartTag() throws JspException {
        ServletContext context = pageContext.getServletContext();
        registrationMap = context.getServletRegistrations();
        iterator = registrationMap.entrySet().iterator();
        if (getNextRegistration()) {
            return EVAL_BODY_INCLUDE;
        }
        return SKIP_BODY;
    }

    @Override
    public int doAfterBody() throws JspException {
        if (getNextRegistration()) {
            return EVAL_BODY_AGAIN;
        }
        return SKIP_BODY;
    }


    private boolean getNextRegistration() throws JspException {
        while (iterator.hasNext()) {
            Map.Entry<String, ? extends  ServletRegistration> registration = (Map.Entry) iterator.next();
            String servlet = registration.getKey();
            Collection<String> mappings = registration.getValue().getMappings();

            for (String mapping : mappings) {
                pageContext.setAttribute(KEY_URL, mapping);
                pageContext.setAttribute(KEY_SERVLET, servlet);
                return true;
            }
        }
        return false;
    }
}
